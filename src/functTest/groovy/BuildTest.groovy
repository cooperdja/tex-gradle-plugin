package org.bitbucket.cooperdja.tex

import spock.lang.*
import org.gradle.testkit.runner.GradleRunner
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import java.nio.file.*

class BuildTest extends Specification
{
    private File projectRoot
    private File buildFile

    def setup()
    {
        projectRoot = Files.createTempDirectory('textest').toFile()
        buildFile = new File(projectRoot, 'build.gradle')        
        buildFile << '''
            plugins {
                id 'org.bitbucket.cooperdja.tex'
            }
        '''
    }
    
    def cleanup()
    {
        projectRoot.deleteDir()
    }
    
    def newFile(String name)
    {
        def file = new File(projectRoot, name)
        file.parentFile.mkdirs()            
        file
    }
    
    def runBuild(String task)
    {
        GradleRunner.create()
            .withProjectDir(projectRoot)
            .withArguments(task)
            .withPluginClasspath()
            .forwardOutput()
            .build()
    }
    
    def pdfText(String pdfFile)
    {
        def pdf = PDDocument.load(new File(projectRoot, pdfFile))
        return pdf.withCloseable { new PDFTextStripper().getText(pdf) }
    }
    
    void checkSuccess(def result, String task)
    {
        assert result.task(':' + task).outcome == org.gradle.testkit.runner.TaskOutcome.SUCCESS
    }
    
    def 'single tex file'()
    {
        given:
            def content = 'xyz'
            def jobname = 'test'
            def docset = 'testdocset'
            
            buildFile << """
                tex {
                    docset('${docset}')
                }
            """
            
            newFile("${docset}/src/${jobname}.tex") << """
                \\documentclass{minimal}
                \\begin{document}
                    ${content}
                \\end{document}
            """
        
        when:
            def result = runBuild(docset)
            def text = pdfText("${docset}/pdf/${jobname}.pdf")
    
        then:
            text.trim() == content
    }
    
    def 'package dependencies'()
    {
        given:
            def content = ["abc", "xyz"]
            def jobname = 'test'
            def docset = 'testdocset'
            def libDir = "lib"
            def pkg = "testpkg"
            def command = "testcommand"
        
            buildFile << """
                dependencies {
                    tex files('${libDir}A/')
                    tex files('${libDir}B/')
                }
            
                tex {
                    docset('${docset}')
                }
            """
            
            newFile("${libDir}A/${pkg}A.sty") << """
                \\ProvidesPackage{${pkg}A}
                \\newcommand{\\${command}A}{${content[0]}}
            """
            
            newFile("${libDir}B/${pkg}B.sty") << """
                \\ProvidesPackage{${pkg}B}
                \\newcommand{\\${command}B}{${content[1]}}
            """
            
            newFile("${docset}/src/${jobname}.tex") << """
                \\documentclass{minimal}
                \\usepackage{${pkg}A}
                \\usepackage{${pkg}B}
                \\begin{document}
                    \\${command}A\\${command}B
                \\end{document}
            """
        
        when:
            def result = runBuild(docset)
            def text = pdfText("${docset}/pdf/${jobname}.pdf")
    
        then:
            text.trim() == content[0] + content[1]
    }
    
    def 'superset hierarchy'()
    {
        given:
            buildFile << """
                tex {
                    superset('setA')
                    {
                        define testcommand: "abc"
                        superset('setAA')
                        {
                            outputFileNames { suffix "_AAA" }
                            define testcommand: "def"
                            docset('setAAA')
                            docset('setAAB')
                            {
                                outputFileNames { suffix "_AAB" }
                                define testcommand: "ghi"
                            }
                        }
                        
                        outputFileNames { suffix "_AB" }
                        docset('setAB')
                        
                        superset('setAC')
                        {
                            outputFileNames { suffix "_ACA" }
                            docset('setACA')
                        }
                    }
                    
                    superset('setB')
                    {
                        docset('setBA')
                        {
                            outputFileNames { suffix "_BA" }
                            dir 'setBA'
                        }
                        
                        docset('setBB')
                        {
                            outputFileNames { suffix "_BB" }
                        }
                    }
                    
                    outputFileNames { suffix "_C" }
                    docset('setC')
                }
            """
            
            for(set in ["A", "B", "BA", "C"])
            {
                for(file in 1..2)
                {
                    newFile("set${set}/src/test${file}.tex") << """
                        \\documentclass{minimal}
                        \\begin{document}
                            test${file}${set} \\ifdefined\\testcommand\\testcommand\\fi
                        \\end{document}
                    """
                }
            }
        
        when:
            def result = runBuild("build")
            def text = [:]
            for(dirset in [["A", "AAA"], ["A", "AAA_AAB"], ["A", "AB"], ["A", "AB_ACA"], ["BA", "BA"], ["B", "BB"], ["C", "C"]])
            {
                def (dir, set) = dirset
                text[set] = [pdfText("set${dir}/pdf/test1_${set}.pdf"),
                             pdfText("set${dir}/pdf/test2_${set}.pdf")]
            }
    
        then:
            text["AAA"    ][0].trim() == "test1A def" 
            text["AAA_AAB"][0].trim() == "test1A ghi" 
            text["AB"     ][0].trim() == "test1A abc" 
            text["AB_ACA" ][0].trim() == "test1A abc" 
            text["BA"     ][0].trim() == "test1BA" 
            text["BB"     ][0].trim() == "test1B"
            text["C"      ][0].trim() == "test1C"
            
            text["AAA"    ][1].trim() == "test2A def" 
            text["AAA_AAB"][1].trim() == "test2A ghi" 
            text["AB"     ][1].trim() == "test2A abc" 
            text["AB_ACA" ][1].trim() == "test2A abc" 
            text["BA"     ][1].trim() == "test2BA" 
            text["BB"     ][1].trim() == "test2B"
            text["C"      ][1].trim() == "test2C"
    }
    
    def 'directive-defined variants'()
    {
        given:
            def content = 'xyz'
            def docset = 'testdocset'
            
            buildFile << """
                tex {
                    docset('${docset}')
                }
            """
            
            newFile("${docset}/src/test.tex") << """
                %!TEX variant:v1 = \\def\\thecommand{variant 1}
                %!TEX variant:v2 = \\def\\thecommand{variant 2}
                \\documentclass{minimal}
                \\begin{document}
                    \\thecommand
                \\end{document}
            """.stripIndent() // The '%' must be in the very first column.
        
        when:
            def result = runBuild(docset)
            def text1 = pdfText("${docset}/pdf/testv1.pdf")
            def text2 = pdfText("${docset}/pdf/testv2.pdf")
    
        then:
            text1.trim() == "variant 1"
            text2.trim() == "variant 2"
            checkSuccess(result, docset)
    }
    
    def 'tex engines'(String engine, String command)
    {
        given:
            buildFile << """
                tex {
                    docset('bydirective')
                    docset('bydeclaration') {
                        texCommand "${command}"
                    }
                }
            """
            
            def tex = """
                \\documentclass{minimal}
                \\usepackage{iftex}
                \\begin{document}
                    \\Require${engine}
                    xyz
                \\end{document}
            """
            
            newFile("bydirective/src/test-directive.tex") << "%!TEX TS-program = ${command}\n${tex}"
            newFile("bydeclaration/src/test-declaration.tex") << tex
            
        when:
            def directiveResult = runBuild('bydirective')
            def declarationResult = runBuild('bydeclaration')
            
        then:
            checkSuccess(directiveResult, 'bydirective')
            checkSuccess(declarationResult, 'bydeclaration')
            
        where:
            engine   | command
            "PDFTeX" | "pdflatex"
            "XeTeX"  | "xelatex"
            "LuaTeX" | "lualatex"
    }
    
    // TEX directives, including standard, customised and variant directives
    
    // Task and output name mapping
    
    
}

