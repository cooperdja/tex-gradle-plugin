package org.bitbucket.cooperdja.tex

import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.*
import java.util.regex.Pattern
import groovy.json.JsonGenerator
import groovy.json.JsonSlurper

@groovy.transform.InheritConstructors
class TexCompileSettingsException extends Exception {}

@groovy.transform.Canonical
class CompileSettings 
{
    @Input
    Configuration configuration

    @Input
    String texCommand
    
    @Input
    String bibCommand
    
    @Input
    List<String> texArgs 
    
    @Input
    List<String> bibArgs
    
    @Input
    String extension
    
    @Input
    Mode mode 
    
    @Console
    Boolean alwaysShowOutput 
    
    @Console
    Boolean silent
    
    @Input
    Integer maxCompileRuns 
    
    @Input
    Boolean failOnCompileLimit 
    
    @Input
    Boolean failOnWarnings 
        
    @Input
    List<Pattern> warningPatterns
    
    @Input
    List<Pattern> rerunPatterns
    
    @Input
    List<Pattern> bibPatterns 
    
    @Input
    String defs   
    
    @Input
    String debugDefs
    
    @Input
    Map<String,String> variantDefs
    
    
    static CompileSettings withDefaults(Project proj)
    {
        def settings = new CompileSettings()
        settings.defaults(proj)
        settings
    }
    
    static CompileSettings blank()
    {
        new CompileSettings()
    }
    
    CompileSettings() {}
    
    void defaults(Project proj)
    {
        configuration = proj.configurations.tex
        texCommand = "xelatex"
        bibCommand = "bibtex"
        texArgs = []
        bibArgs = []
        extension = "pdf"
        mode = Mode.NONSTOP
        alwaysShowOutput = false
        silent = false
        maxCompileRuns = 3
        failOnCompileLimit = true
        failOnWarnings = true
        warningPatterns = [~/\n((LaTeX Warning|Underfull|Overfull)[^\n]*)/]
        rerunPatterns = [~"Rerun to get"]
        bibPatterns = [~"There were undefined references"]
        defs = ""   
        debugDefs = ""
        variantDefs = [:]
    }
    
    CompileSettings clone()
    {
        def copy = new CompileSettings()
        copy.configuration = configuration
        copy.texCommand = texCommand
        copy.bibCommand = bibCommand
        copy.texArgs = texArgs.clone()
        copy.bibArgs = bibArgs.clone()
        copy.extension = extension
        copy.mode = mode
        copy.alwaysShowOutput = alwaysShowOutput
        copy.silent = silent
        copy.maxCompileRuns = maxCompileRuns
        copy.failOnCompileLimit = failOnCompileLimit
        copy.failOnWarnings = failOnWarnings
        copy.warningPatterns = warningPatterns.clone()
        copy.rerunPatterns = rerunPatterns.clone()
        copy.bibPatterns = bibPatterns.clone()
        copy.defs = defs
        copy.debugDefs = debugDefs
        copy.variantDefs = variantDefs.clone()
        copy
    }
    
    static JsonGenerator serialiser = new JsonGenerator.Options()
        .addConverter(Pattern) { Pattern p -> p.pattern() }
        .addConverter(Mode) { Mode m -> m.optionName }
        .excludeFieldsByType(JsonGenerator)
        .build()
    
    void write(File settingsFile)
    {    
        settingsFile.text = serialiser.toJson(this);
    }
    
    CompileSettings read(File settingsFile)
    {
        def json = new JsonSlurper().parse(settingsFile);
        texCommand = json.texCommand 
        bibCommand = json.bibCommand
        texArgs = json.texArgs 
        bibArgs = json.bibArgs 
        extension = json.extension
        mode = Mode[json.mode]
        alwaysShowOutput = json.alwaysShowOutput
        silent = json.silent
        maxCompileRuns = json.maxCompileRuns
        failOnCompileLimit = json.failOnCompileLimit
        failOnWarnings = json.failOnWarnings
        warningPatterns = json.warningPatterns.collect { String s -> ~s } // Convert strings back to regexes
        rerunPatterns = json.rerunPatterns.collect { String s -> ~s }
        bibPatterns = json.bibPatterns.collect { String s -> ~s }
        defs = json.defs
        debugDefs = json.debugDefs
        variantDefs = json.variantDefs 
        this
    }
        
    CompileSettings updateFrom(CompileSettings other)
    {
        def update       = { p -> this."$p" = (other."$p" == null ? this."$p" : other."$p") }
        def updateConcat = { p -> this."$p" = (this."$p" ?: "" ) + (other."$p" ?: "" ) }
        def updateMap    = { p -> this."$p" = (this."$p" ?: [:]) + (other."$p" ?: [:]) }
        def updateList   = { p -> this."$p" = (this."$p" ?: [] ) + (other."$p" ?: [] ) }
    
        update('texCommand')
        update('bibCommand')
        updateList('texArgs')
        updateList('bibArgs')
        update('extension')
        update('mode')
        update('alwaysShowOutput')
        update('silent')
        update('maxCompileRuns')
        update('failOnCompileLimit')
        update('failOnWarnings')
        updateList('warningPatterns')
        updateList('rerunPatterns')
        updateList('bibPatterns')
        updateConcat('defs')
        updateConcat('debugDefs')
        updateMap('variantDefs')
        this
    }
    
    void texCommand(String cmd)
    {
        this.texCommand = cmd
    }
    
    void bibCommand(String cmd)
    {
        this.bibCommand = cmd
    }
    
    void texArg(String... arg)
    {
        texArgs += arg
    }
    
    void bibArg(String... arg)
    {
        bibArgs += arg
    }
    
    void extension(String ext)
    {
        this.extension = extension
    }
    
    void mode(String m)
    {
        if(!Mode[m])
        {
            throw new TexCompileSettingsException("Invalid tex mode: '$m'")
        }
        this.mode = Mode[m]
    }
    
    void mode(Mode m)
    {
        this.mode = m
    }
    
    void alwaysShowOutput(boolean show)
    {
        this.alwaysShowOutput = show
    }
    
    void silent(boolean hide)
    {
        this.silent = hide
    }
    
    void maxCompileRuns(int max)
    {
        this.maxCompileRuns = max
    }
    
    void failOnCompileLimit(boolean fail)
    {
        this.failOnCompileLimit = fail
    }
    
    void failOnWarnings(boolean fail)
    {
        this.failOnWarnings = fail
    }
    
    void warning(String msg)
    {
        warnings.add(Pattern.compile(Pattern.quote(msg)))
    }
    
    void warning(Pattern warningPattern)
    {
        warnings.add(warningPattern)
    }
    
    void rerunPattern(String output)
    {   
        rerunPatterns.add(Pattern.compile(Pattern.quote(output)))
    }
    
    void rerunPattern(Pattern outputPattern)
    {
        rerunPatterns.add(outputPattern)
    }
    
    void bibPattern(String output)
    {
        bibPatterns.add(Pattern.compile(Pattern.quote(output)))
    }
    
    void bibPattern(Pattern outputPattern)
    {
        bibPatterns.add(outputPattern)
    }
    
    private String makeTexDefs(Map<String,String> defMap)
    {
        defMap.collect 
        { 
            macroName, value -> 
            if(!(macroName ==~ ~/[A-Za-z@]+/))
            {
                throw new TexCompileSettingsException("'${macroName}' is not a valid TeX macro name'")
            }
            def tex = (value as String).replaceAll('[#$%&_{}]', '\\$0')
                                       .replaceAll('\\\\', '\\textbackslash{}')
                                       .replaceAll('\\^', '\\textasciicircum{}')
                                       .replaceAll('~', '\\textasciitilde{}')
            "\\gdef\\${macroName}{${tex}}"
        }.join()
    }
    
    void define(String tex)
    {
        defs += tex
    }
    
    void define(Map<String,String> defMap)
    {
        defs += makeTexDefs(defMap)
    }
    
    void variant(String key, String tex)
    {
        if(variantDefs == null)
        {
            variantDefs = [:]
        }
        variantDefs[key] = (variantDefs[key] ?: "") + tex
    }
    
    void variant(String key, Map<String,String> defMap)
    {
        variant(key, makeTexDefs(defMap))
    }
    
    void debugOnWarnings(String tex)
    {
        debugDefs += tex
    }
    
    void debugOnWarnings(Map<String,String> defMap)
    {
        debugDefs += makeTexDefs(defMap)
    }
}
