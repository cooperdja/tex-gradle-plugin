package org.bitbucket.cooperdja.tex

import org.gradle.api.*
import org.gradle.api.file.ConfigurableFileCollection;
import org.gradle.api.tasks.*

import java.nio.file.*

@groovy.transform.InheritConstructors
class TexCompileException extends Exception {}

class TexCompile extends DefaultTask
{   
    @Nested
    CompileSettings compileSettings = null

    @InputFile
    File parsedSettings
    
    @InputFile
    File source    
    
    @InputFiles
    ConfigurableFileCollection extraDeps
    
    @Input
    String jobName
    
    // Note: this is *input information* on which the output files are based, not (itself) an output.
    @Input
    File outDir
    
    // We declare (potentially) multiple targets because we could be asked to compile several 
    // versions of the same document, as per CompileSettings.
    @OutputFiles
    List<File> targets = []

    @OutputDirectory
    File tmpDir   
    
    private Closure processBuilderFactory = { new ProcessBuilder() }
    
    void processBuilderFactory(Closure factory)
    {
        processBuilderFactory = factory
    }
                
    @TaskAction
    def runTex() 
    {
        def readSettings = CompileSettings.blank().read(parsedSettings)
        def finalSettings = compileSettings.clone().updateFrom(readSettings)
        
        if(!outDir.exists() && !outDir.mkdirs())
        {
            throw new TexCompileException("Could not create output directory '$outDir'.") 
        }
        
        if(finalSettings.variantDefs)
        {
            targets = []
            def baseDefs = finalSettings.defs
            for(entry in finalSettings.variantDefs)
            { 
                def variantJobName = jobName + entry.key
                finalSettings.defs = baseDefs + entry.value
                compileVariant(variantJobName, finalSettings)
                targets += new File(outDir, variantJobName + finalSettings.extension)
            }
        }
        else
        {
            compileVariant(jobName, finalSettings)
            targets = [new File(outDir, jobName)]
        }
    }
    
    
    private static void archiveTmpDir(File jobTmpDir)
    {
        // When the compile fails, we want to start again with a fresh slate, without all the 
        // temporary files created during the failed attempt. Keeping them around may interfere 
        // with the next attempt.
        
        // However, we don't just want to delete them, because (from time to time) they might 
        // contain important debugging details.
        
        // Instead, we "archive" them, moving the temporary files to another directory.
        
        def archivedTmpDir = new File(jobTmpDir.absoluteFile.parentFile, jobTmpDir.name + ".failed")
        
        if(!archivedTmpDir.exists() && !archivedTmpDir.mkdirs())
        {
            throw new TexCompileException("Could not create temporary directory '$archivedTmpDir'.")
        }
        
        if(archivedTmpDir.isDirectory())
        {
            for(tmpFile in jobTmpDir.listFiles())
            {
                def archivedTmpFile = new File(archivedTmpDir, tmpFile.name)
                if(!tmpFile.renameTo(archivedTmpFile))
                {
                    throw new TexCompileException("Cannot rename '$tmpFile' to '$archivedTmpFile'.")
                }
            }
        }
        else 
        {
            throw new TexCompileException("Expected '$archivedTmpDir' to be a directory or non-existent, but it isn't.")
        }
    }
    
    private void showTexOutput(List<String> texCommand, String texInputs, String texOutput, CompileSettings settings)
    {
        if(!settings.silent)
        {
            def highlightedOutput = texOutput
            for(pattern in settings.warningPatterns)
            {
                highlightedOutput = highlightedOutput.replaceAll(pattern, '\n\033[33m$1\033[0m')
            }
        
            println "TEXINPUTS=$texInputs\n$texCommand\n"
        
            // Print tex output, highlighting errors
            println highlightedOutput
                // - Matches driver errors
                .replaceAll(/\n((!|xdvipdfmx:fatal)[^\n]*)/, '\n\n\033[31;1m$1\033[0m')
                
                // - Matches 'Runaway argument\n...' messages
                .replaceAll(/\n(Runaway argument\?\s*\n[^\n]*)/, '\n\n\033[31;1m$1\033[0m')
                
                // - Matches 'file:line:error' error messages: /\n(([^:\n]*\/)([^:\n\/]*):([0-9]+):([^\n.]*[^.]\n)*([^\n]+))/
                .replaceAll(/\n([^:\n]*\/)([^:\n\/]*):([0-9]+):\s*(([^\n.]*[^.]\n)*([^\n]+))/, 
                {
                    def path = it[1]
                    def file = it[2]
                    def line = it[3]
                    def error = it[4].replaceAll("\n", "")
                    "\n\n\033[1m[$path]\n\033[35;1m$file\033[37;1m:$line: \033[31;1m$error\033[0m\n"
                })
        }
    }
    
    private void moveFinalDocument(String jobName, CompileSettings settings)
    {
        // Move PDF to its final location
        def targetName = "${jobName}.${settings.extension}"
        def tmpTarget = tmpDir.toPath().resolve(jobName).resolve(targetName)
        def finalTarget = outDir.toPath().resolve(targetName)
        Files.move(tmpTarget, finalTarget, StandardCopyOption.REPLACE_EXISTING);       
    }

    private void recompileDebug(String jobName, CompileSettings settings)
    {
        // If debug settings have been specified, re-run with those settings. For this, we
        // switch off output, because the relevant messages have already been output. 
        
        // We expect that this will throw an exception, though technically it might not, 
        // because the debug settings could change the compilation process enough that the
        // underlying TeX warning doesn't happen anymore.
        
        if(!settings.debugDefs)
        {
            throw new IllegalStateException();
        }

        if(!settings.silent)
        {
            println "\033[31mRe-compiling debug version due to warning(s), using '${settings.debugDefs}'.\033[0m\n"
        }
        def debugSettings = settings.clone()
        debugSettings.defs += debugSettings.debugDefs
        debugSettings.debugDefs = "" // Ensure we don't recurse forever
        debugSettings.silent = true
        compileVariant(jobName, debugSettings)                
    }
    
    private void compileVariant(String jobName, CompileSettings finalSettings)
    {
        File sourceDir = source.parentFile
        File jobTmpDir = new File(tmpDir, jobName)
        if(!jobTmpDir.isDirectory() && !jobTmpDir.mkdirs())
        {
            throw new TexCompileException("Could not create temporary output directory '$jobTmpDir'.")
        }
    
        // Setting up tex command
        def texCommand = [
            finalSettings.texCommand, 
            "-jobname", jobName,
            "-output-directory", jobTmpDir.getAbsolutePath(), 
            "-interaction=${finalSettings.mode.optionName}mode",
            "-file-line-error",
            "-halt-on-error",
            *finalSettings.texArgs, 
            "${finalSettings.defs}\\input{${source.absolutePath}}"]
        def texProcessBuilder = processBuilderFactory().command(*texCommand)
            
        // Sort through the dependencies:
        // (a) Directories should be added straight to TEXINPUTS below.
        // (b) Individual files are copied into the output (tmp) directory, where TeX can find 
        // them. I don't know of a way to feed them directly to TeX, without just taking their 
        // parent directories (which may defeat certain filtering the build author intends to do).
        def dependencyDirs = finalSettings.configuration.findResults {
            file ->
            if(file.isDirectory())
            {
                // Note: the trailing double-slash makes TeX search the directory recursively. We
                // assume this is the most useful behaviour!
                return file.absolutePath + "//"
            }
            else
            {
                def source = file.toPath()
                def target = jobTmpDir.toPath().resolve(file.name)
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING)
                return null // Don't need to add to dependencyDirs
            }
        }
        
        // TEXINPUTS should be "sourceDir:[libDir1[:libdir2...]]:" (where the empty component at the end is 
        // replaced with the standard library location).
        def texInputs = (
            sourceDir.absolutePath +
            File.pathSeparatorChar +
            dependencyDirs.join(File.pathSeparator) +
            File.pathSeparatorChar
        )
        texProcessBuilder.environment().TEXINPUTS = texInputs
        
        // Try not to split console output lines, especially because it screws up our ability to parse error messages.
        texProcessBuilder.environment().max_print_line = "10000"
        texProcessBuilder.redirectErrorStream(true)
        
        // Run tex, capture the output, and wait for it to finish
        Process proc = texProcessBuilder.start()
        String texOutput = proc.getInputStream().text
        int exitCode = proc.waitFor()
        int run = 1
        
        // Check if bibtex (or equivalent) needs to be run
        if(exitCode == 0 && finalSettings.bibPatterns.any { texOutput =~ it })
        {
            def bibProcessBuilder = processBuilderFactory().command(
                finalSettings.bibCommand, 
                *finalSettings.bibArgs,
                jobName)
            bibProcessBuilder.environment().BIBINPUTS = sourceDir.absolutePath
            bibProcessBuilder.directory(jobTmpDir)
            proc = bibProcessBuilder.start()            
            String bibOutput = proc.getInputStream().text
            exitCode = proc.waitFor()
            
            if(bibOutput.contains("I found no \\citation commands"))
            {
                // Ignore this one -- it just means we didn't really have to run BibTex after all.
                exitCode = 0
            }
            
            if(exitCode != 0)
            {
                if(!finalSettings.silent)
                {
                    println bibOutput
                }
                archiveTmpDir(jobTmpDir)
                throw new TexCompileException("$source: ${finalSettings.bibCommand} returned $exitCode.")
            }
        }

        // Rerun tex if needed (for cases where it needs several passes)
        while(exitCode == 0 && run <= finalSettings.maxCompileRuns && (
            finalSettings.rerunPatterns.any { texOutput =~ it } ||
            finalSettings.bibPatterns.any { texOutput =~ it }))
        {
            proc = texProcessBuilder.start()
            texOutput = proc.getInputStream().text
            exitCode = proc.waitFor()
            run += 1
        }
        
        // Was it successful?
        boolean errors = (exitCode != 0)
        boolean warnings = 
            finalSettings.warningPatterns.any { texOutput =~ it }
        boolean compileLimit = 
            finalSettings.failOnCompileLimit &&
            (run > finalSettings.maxCompileRuns)
                 
        if(errors)
        {
            showTexOutput(texCommand, texInputs, texOutput, finalSettings)
            archiveTmpDir(jobTmpDir)
            throw new TexCompileException("$source: ${finalSettings.texCommand} $msg.")
        }
        else if((finalSettings.failOnWarnings && warnings) || 
                (finalSettings.failOnCompileLimit && compileLimit))
        {
            showTexOutput(texCommand, texInputs, texOutput, finalSettings)
            def msg
            if(warnings)
            {
                msg = "generated warning message(s), and 'failOnWarnings' is true"
            }
            else
            {
                def condition = finalSettings.rerunPatterns.any { texOutput =~ it } ?
                    "re-running" : "bibliography regeneration"
                
                msg = "continues to require $condition after $run compile runs, and 'failOnCompileLimit' is true"
            }
            
            if(finalSettings.debugDefs)
            {
                archiveTmpDir(jobTmpDir)
                recompileDebug(jobName, finalSettings)
            }
            else
            {
                moveFinalDocument(jobName, finalSettings)
                archiveTmpDir(jobTmpDir)
            }
            
            throw new TexCompileException("$source: ${finalSettings.texCommand} $msg.")
        } 
        else if((warnings || compileLimit) && finalSettings.debugDefs)
        {
            // Note: at this point, failOnWarnings and failOnCompileLimit must be FALSE, so this 
            // isn't formally an error, unless the debug re-run itself causes an error.
            if(finalSettings.alwaysShowOutput)
            {
                showTexOutput(texCommand, texInputs, texOutput, finalSettings)
            }
            archiveTmpDir(jobTmpDir)
            recompileDebug(jobName, finalSettings)
        }
        else
        {
            // Finally, nothing went wrong!
            if(finalSettings.alwaysShowOutput)
            {
                showTexOutput(texCommand, texInputs, texOutput, finalSettings)
            }
            moveFinalDocument(jobName, finalSettings)
        }
    }
}
