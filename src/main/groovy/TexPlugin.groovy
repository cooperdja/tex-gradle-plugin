package org.bitbucket.cooperdja.tex

import org.gradle.api.*
import org.gradle.api.tasks.*

class TexPlugin implements Plugin<Project> 
{
    void apply(Project project) 
    {
        project.apply(plugin: "base")
        project.configurations.create('tex')
        project.extensions.create("tex", TexDeclarator, project, project.assemble)
    }
}
