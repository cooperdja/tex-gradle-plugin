package org.bitbucket.cooperdja.tex

import groovy.io.FileType;
import org.gradle.api.*
import org.gradle.api.tasks.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.regex.Pattern


class TexDeclarator
{
    static final String SHORTHAND_PREFIX = "*"
    static final Logger logger = LoggerFactory.getLogger(TexDeclarator.getName())

    Project project
    List<TexSettings> settingStack
    List<Task> taskStack   
    
    @Delegate
    TexSettings globalSettings
    
    @Delegate
    CompileSettings globalCompileSettings 
    
    private boolean inDocset = false
    
    TexDeclarator(Project proj, Task topLevelTask)
    {
        this.project = proj
        this.globalSettings = TexSettings.withDefaults(proj)
        this.globalCompileSettings = this.globalSettings.compileSettings
        this.taskStack = [topLevelTask]
        this.settingStack = [globalSettings]
    }
        
    private String qualifyName(String newText, String context, String description)
    {
        def mangle = false
        if(!newText)
        {
            newText = description
            mangle = true
        }
        
        def shorthand = newText.startsWith(SHORTHAND_PREFIX)
        if(shorthand)
        {
            newText = newText.substring(SHORTHAND_PREFIX.length())
        }

        if(mangle)
        {
            // Create a Java-style identifier based on free-form text:
            // - capitalise any lowercase letter not preceeded another letter
            // - then remove any non-alphanumeric character
            newText = newText
                .replaceAll(~/(?<![a-zA-Z])[a-z]/, { it[0].toUpperCase() })
                .replaceAll(~/[^a-zA-Z0-9_]/, "")
                
            // Make the first character lowercase
            newText = newText.substring(0, 1).toLowerCase() + newText.substring(1)
        }
        
        if(shorthand)
        {
            newText = context + newText.substring(0, 1).toUpperCase() + newText.substring(1)
        }
        else
        {
            newText = newText
        }
        return newText
    }
    
    private String qualifyDescription(String newText, String context)
    {
        if(newText == null)
        {
            return ""
        }
        else if(newText.startsWith(SHORTHAND_PREFIX))
        {
            return context + ": " + newText.substring(SHORTHAND_PREFIX.length())
        }
        else
        {
            return newText
        }
    }
    
    private File matchDirectory(String name)
    {
        // First see if 'name' is an exact match for a subdirectory in the project directory.        
        File dir = new File(project.projectDir, name)
        
        if(dir.isDirectory())
        {
            return dir
        }
        else
        {
            // Fallback -- try to find a subdirectory whose name is a case-insensitive match.
            
            def matches = null        
            dir = project.projectDir.eachDir 
            { 
                d ->
                if(!matches && d.name.equalsIgnoreCase(name))
                {
                    matches = d
                }
            }
            return matches
            // This returns null if matches is empty
        }
    }

    void superset(String description, Closure closure)
    {
        superset(null, description, closure)
    }

    void superset(String supersetName, String supersetDescription, Closure closure)
    {
        if(inDocset)
        {
            String msg = "'superset' cannot be nested inside 'docset'"
            logger.error(msg)
            throw new InvalidUserDataException(msg)
        }
        
        def lastSettings = settingStack.last()
        def settings = settingStack.last().clone()
        
        supersetName = qualifyName(supersetName, lastSettings.name, supersetDescription)
        supersetDescription = qualifyDescription(supersetDescription, lastSettings.description)
        settings.name = supersetName
        settings.description = supersetDescription
        if(!settings.directory)
        {
            settings.directory = matchDirectory(supersetName)
        }        
        settingStack.add(settings)

        def newSuperTask = project.task(supersetName)
        {
            description = supersetDescription
            group = "tex"
        }
        taskStack.last().dependsOn newSuperTask
        taskStack.add(newSuperTask)
        
        closure.delegate = settings
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        closure()        
        taskStack.removeLast()
        settingStack.removeLast()
    }
    
    void docset(String description, Closure closure = null)
    {
        docset(null, description, closure)
    }
    
    void docset(String docsetName, String docsetDescription, Closure closure = null)
    {
        if(inDocset)
        {
            String msg = "'docset' cannot be nested inside another 'docset'"
            logger.error(msg)
            throw new InvalidUserDataException(msg)
        }
        
        def lastSettings = settingStack.last()
        def settings = lastSettings.clone()
        docsetName = qualifyName(docsetName, lastSettings.name, docsetDescription)        
        docsetDescription = qualifyDescription(docsetDescription, lastSettings.description)
        settings.name = docsetName
        settings.description = docsetDescription
        
        if(closure != null)
        {
            closure.delegate = settings
            closure.resolveStrategy = Closure.DELEGATE_ONLY
            inDocset = true
            closure()
            inDocset = false
        }
        
        if(!settings.directory)
        {
            settings.directory = matchDirectory(docsetName)
            if(!settings.directory)
            {
                String msg = "No directory found for '$docsetName'."
                logger.error(msg)
                throw new InvalidUserDataException(msg)
            }
        }
        
        File docsetDir = project.file(settings.directory, PathValidation.DIRECTORY)
        File theTmpDir = new File(this.project.buildDir, "textmp/$docsetName")
        File parsedSettingsDir = new File(this.project.buildDir, "textmp/parsed_directives")
        File theOutDir = new File(docsetDir, "pdf")
        
        List<Pattern> includePatterns = settings.includes
        List<Pattern> excludePatterns = settings.excludes
        
        def docsetTask = project.task(docsetName) 
        { 
            description = docsetDescription
            group = "tex" 
        }
        
        def cleanTask = project.task([type: Delete], "${docsetName}Clean")
        {
            delete theTmpDir
        }
        
        this.taskStack.last().dependsOn docsetTask
        this.project.clean.dependsOn cleanTask
        
        // Iterate through all source files, and create a task for each one
        File srcDir = new File(docsetDir, "src")
        if(!srcDir.isDirectory())
        {
            String msg = "\"$srcDir\" does not exist, or is not a directory"
            logger.error(msg)
            throw new InvalidUserDataException(msg)
        }
        
        srcDir.eachFileMatch FileType.FILES, ~/.*\.tex/,
        {
            texFile ->
            
            // ... Provided it conforms to the include and exclude patterns.
            if((includePatterns.isEmpty() || includePatterns.any { texFile =~ it } )
                                          && !excludePatterns.any { texFile =~ it } )
            {
                def baseFileName = texFile.name.replaceFirst(/\.tex$/, "")
                
                // Map the original filename to a base task name.
                def baseTaskName = settings.inputTaskNameMapping.map(baseFileName)
                
                // A preprocess task will extract information from the source file in preparation
                // for any 
                def parseDirectivesTaskName = "preprocess_${baseTaskName}"
                def parseDirectivesTask
                def parsedSettingsFile = new File(parsedSettingsDir, 
                    texFile.name.replace('/', '_').replace('\\', '_') + ".directives");
                
                // Obtain (and create if necessary) the preprocess task, as well as an umbrella 
                // task that entails all tasks related to this particular source file.
                def fileTask
                try
                {
                    fileTask = this.project.tasks.getByName(baseTaskName)
                    parseDirectivesTask = this.project.tasks.getByName(parseDirectivesTaskName)
                }
                catch(UnknownTaskException e)
                {
                    fileTask = this.project.task(baseTaskName)
                    {
                        group = "tex file"
                    }
                    
                    parseDirectivesTask = this.project.task([type: ParseDirectives], parseDirectivesTaskName)
                    {
                        source = texFile
                        parsedSettings = parsedSettingsFile
                    }
                }                
                
                def docTaskName = docsetName + "_" + baseTaskName
                
                def theExtraDeps = settings.extraDeps.collect
                {
                    def fullDep = it
                    if(it in String && it.charAt(0) != File.separatorChar)
                    {   
                        fullDep = "${srcDir}${File.separatorChar}${it}"
                    }
                    fullDep
                }
                
                def docTask = this.project.task([type: TexCompile], docTaskName)
                {
                    compileSettings = settings.compileSettings
                    parsedSettings = parsedSettingsFile
                    
                    source = texFile
                    extraDeps = project.files(*theExtraDeps)
                    jobName = settings.outputFileNameMapping.map(baseFileName)
                    tmpDir = theTmpDir
                    outDir = theOutDir
                }
                            
                docTask.dependsOn parseDirectivesTask
                docsetTask.dependsOn docTask
                fileTask.dependsOn docTask
            }
        }
    }
}
