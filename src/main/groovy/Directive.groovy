package org.bitbucket.cooperdja.tex

import org.gradle.api.InvalidUserDataException
import java.util.regex.Pattern


/* 
TODO:
* Tokenise TS-program, to retrieve arguments
* Check each argument to see if it represents one of the settings in CompileSettings; and if so, set *that* instead of texArg().
*/

@groovy.transform.Canonical
class Directive
{
    Pattern typePattern = ~/(?i)TEX/
    Pattern keyPattern
    Closure action
    
//     static List<Directive> DEFAULTS = [PROGRAM, PARAMETER, BIB_PROGRAM, BIB_PARAMETER, VARIANTS]
//     
    // %!TEX TS-program = <command>
    // %!TEX program = <command>
    static Directive PROGRAM = new Directive(
        keyPattern: ~/(?i)^((TS-)program|engine)$/, 
        action: { t, k, v -> texCommand(v) }
    )
//        
//     // %!TEX TS-parameter = <command-line-param>
//     // %!TEX parameter = <command-line-param>
//     static Directive PARAMETER = new Directive(
//         keyPattern: ~/(?i)^((TS-)?parameter|options)$/,
//         action: { t, k, v -> texArg(*v.split()) }
//     )
// 
//     // %!BIB TS-program = <command>
//     // %!BIB program = <command>
//     // Notes:
//     // (a) I have not seen '!BIB engine', but this is an extrapolation from '!TEX engine' in
//     //     atom-latex.
//     // (b) TeXstudio also uses '!TeX TXS-program:bibliography'. However, this expects the value to be
//     // a URI of the form txs:///<id>, which doesn't have any natural meaning outside of TeXstudio. 
//     // Thus, this won't be supported here.)
//     static Directive BIB_PROGRAM = new Directive(
//         type: ~/(?i)^BIB$/,
//         keyPattern: ~/(?i)^((TS-)program|engine)$/, 
//         action: { t, k, v -> bibCommand(v) }
//     )
//        
//     // %!BIB parameter = <command-line-param>
//     // (I haven't seen this one documented anywhere, but it's a logical extrapolation of the above.)
//     static Directive BIB_PARAMETER = new Directive(
//         type: ~/(?i)^BIB$/,
//         keyPattern: ~/(?i)^((TS-)?parameter|options)$/,
//         action: { t, k, v -> bibArg(*v.split()) }
//     )
//         
    // %!TEX variant:<name> = <texcode>
    // A novel directive giving a snippet of tex code for generating a particular document variant.
    // Normally this directive would be given multiple times (to get multiple variants).
    static Directive VARIANTS = new Directive(
        keyPattern: ~/(?i)^variant:(?<name>.*)$/,
        action: { t, k, v -> variant(k.group('name'), v) }
    )
 
    
    // See:
    // * https://tex.stackexchange.com/questions/78101/when-and-why-should-i-use-tex-ts-program-and-tex-encoding
    // * http://www.tug.org/TUGboat/tb32-1/tb100wright-texworks.pdf
    
    // By these sources, other quasi-standard directives include:
    // !TEX encoding = <encoding>
    // !TEX root = <filename.tex>
    // !TEX spellcheck = <languagespec>
    // !BIB TS-program = <command>
    
    // From Texshop (https://pages.uoregon.edu/koch/texshop/changes_3.html):
    // !TEX parameter = <param>
    // !TEX pdfSinglePage
    // !TEX useOldSyncParser
    // !TEX useTabs (one, two, , short name, five)
    // !TEX useTabsWithFiles
    // !TEX tabbedFile{chapter1/Introduction.tex} (One)
    // !TEX fileForTab = 
    
    // From TexStudio (http://texstudio.sourceforge.net/manual/current/usermanual_en.html):
    // !TeX TXS-program:bibliography = txs:///biber
    // !TeX TXS-SCRIPT = foobar
    // ... // JavaScript Code
    // TXS-SCRIPT-END
    
    // From LatexTools (SublimeText) (https://latextools.readthedocs.io/en/latest/features/):
    // !TEX program =   (rather than TS-program)
    // !TEX options =   (rather than parameter)
    // !TEX output_directory =
    // !TEX jobname = 
    
    // From atom-latex (https://github.com/thomasjo/atom-latex/wiki/Overridding-Build-Settings):
    // !TEX cleanPatterns
    // !TEX enableSynctex
    // !TEX enableExtendedBuildMode
    // !TEX enableShellEscape
    // !TEX engine      (alternative to program)
}
