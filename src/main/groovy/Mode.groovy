package org.bitbucket.cooperdja.tex

enum Mode
{
    BATCH, NONSTOP, SCROLL, ERRORSTOP;
    
    static final Map<String,Mode> map = values().collectEntries { [it.optionName, it] }

    static Mode getAt(String s)
    {
        return map[s]
    }

    String getOptionName()
    {
        name().toString().toLowerCase()
    }
}
