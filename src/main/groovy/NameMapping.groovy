package org.bitbucket.cooperdja.tex

import org.gradle.api.InvalidUserDataException
import java.util.regex.Pattern

class NameMapping
{
    private def Closure mapping
    
    static NameMapping identity()
    {
        return new NameMapping(Closure.IDENTITY)
    }

    NameMapping(Closure mapping) 
    {
        this.mapping = mapping
    }
    
    NameMapping clone()
    {
        new NameMapping(mapping)
    }
    
    void add(Closure clos)
    {
        clos.delegate = this
        clos.resolveStrategy = Closure.DELEGATE_FIRST
        clos()
    }
    
    void delete(String delSpec)
    {
        mapping = mapping >> {it.replace(delSpec, "")}
    }
    
    void delete(Pattern delSpec)
    {
        mapping = mapping >> {it.replaceFirst(delSpec, "")}
    }
    
    void replace(String replSpec, String with)
    {
        mapping = mapping >> {it.replace(replSpec, with)}
    }

    void replace(Pattern replSpec, String with)
    {
        mapping = mapping >> {it.replaceFirst(replSpec, with)}
    }
            
    void prefix(String prefixSpec)
    {
        mapping = mapping >> {prefixSpec + it}
    }
    
    void suffix(String suffixSpec)
    {
        mapping = mapping >> {it + suffixSpec}
    }
    
    String map(String name)
    {
        return mapping(name)
    }
}
