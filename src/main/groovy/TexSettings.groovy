package org.bitbucket.cooperdja.tex

import org.gradle.api.Project
import java.util.regex.Pattern

@groovy.transform.Canonical
class TexSettings 
{
    @Delegate
    CompileSettings compileSettings
    
    List<Directive> directives
    
    NameMapping inputTaskNameMapping
    NameMapping outputFileNameMapping 
    
    String name 
    String description
    Object directory
    List<Pattern> includes
    List<Pattern> excludes
    List<Object> extraDeps
    
    
    static TexSettings withDefaults(Project proj)
    {
        def settings = new TexSettings()
        settings.defaults(proj)
        settings
    }
    
    void defaults(Project proj)
    {
        compileSettings = CompileSettings.withDefaults(proj)
        directives = []
        inputTaskNameMapping = NameMapping.identity()
        outputFileNameMapping = NameMapping.identity()
        name = ""
        description = null
        directory = null
        includes = []
        excludes = []
        extraDeps = []
    }
    
    TexSettings() {}

    TexSettings clone()
    {
        def copy = new TexSettings()
        copy.compileSettings = compileSettings.clone()
        copy.directives = directives.clone()
        copy.inputTaskNameMapping = inputTaskNameMapping.clone()
        copy.outputFileNameMapping = outputFileNameMapping.clone()
        copy.name = name
        copy.description = description
        copy.directory = directory
        copy.includes = includes.clone()
        copy.excludes = excludes.clone()
        copy.extraDeps = extraDeps.clone()
        copy
    }
    
    private Pattern toPattern(String thing, Object obj)
    {
        Pattern p;
        switch(obj)
        {
            case Pattern: p = obj; break;
            case String:  p = ~obj; break;
            default: 
                throw new IllegalArgumentException("$thing must be a string or a regex")
        }
        return p;
    }
    
    void directive(Object type, Object key, Closure action)
    {
        directives += new Directive(typePattern: toPattern("type", type), 
                                    keyPattern: toPattern("key", key), 
                                    action: action)
    }
    
    void directive(Object key, Closure action)
    {
        directives += new Directive(keyPattern: toPattern("key", key), 
                                    action: action)
    }
    
    void dir(Object newDir)
    {
        this.directory = newDir
    }
    
    void include(Pattern inc)
    {
        includes.add(toPattern("include", inc))
    }
    
    void exclude(Pattern exc)
    {
        excludes.add(toPattern("exclude", exc))
    }
    
    void inputTaskNames(Closure clos)
    {
        inputTaskNameMapping.add(clos)
    }
    
    void outputFileNames(Closure clos)
    {
        outputFileNameMapping.add(clos)
    }
        
    void uses(Object... dependencies)
    {
        dependencies.each { extraDeps << it }
    }
    
}
