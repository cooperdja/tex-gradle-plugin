package org.bitbucket.cooperdja.tex

import org.gradle.api.*
import org.gradle.api.tasks.*
import java.util.regex.*

class ParseDirectives extends DefaultTask
{   
    @Input
    boolean onlyAtStart = true
    
    @Input
    Pattern directivePattern = ~/(?x)
        ^\s*%\s*            # Starts with a % in the very first column
        !(?<type>\S+)       # The directive type. Almost always 'TEX' in practice.
        \s*             
        (?<key>[^\s=]+)     # The directive name: any non-space chars except '='.
        (
            \s*=\s*
            (?<value>.*\S)? # The optional directive value: any char(s), ending in a non-space.
        )?
        \s*                 # Ignore trailing whitespace
        $/
        
    @Input
    Pattern operativeLine = ~/^\s*[^%]/ // An 'operative' line is a non-blank, non-comment line.

//     @Input
    List<Directive> directives = [Directive.PROGRAM, Directive.VARIANTS] //Directive.DEFAULTS.clone()

    @InputFile
    File source    
    
    @OutputFile
    File parsedSettings
    
    @TaskAction
    def parse() 
    {
        def settings = CompileSettings.blank()
        
        for(line in source.readLines())
        {
            def matcher = (line =~ directivePattern)
            if(matcher)
            {
                def type = matcher.group('type')
                def key = matcher.group('key')
                def value = matcher.group('value')
                
                for(directive in directives)
                {
                    Matcher typeMatcher = (type =~ directive.typePattern)
                    Matcher keyMatcher = (key =~ directive.keyPattern)
                    
                    if(typeMatcher && keyMatcher)
                    {
                        directive.action.delegate = settings
                        directive.action.resolveStrategy = Closure.DELEGATE_ONLY
                        directive.action(typeMatcher, keyMatcher, value)
                    }
                }
            }
            else if(onlyAtStart && line =~ operativeLine)
            {
                break
            }
        }
        
        settings.write(parsedSettings)
    }
}
